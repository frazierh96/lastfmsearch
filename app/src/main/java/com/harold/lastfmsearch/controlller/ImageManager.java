package com.harold.lastfmsearch.controlller;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.util.LruCache;
import android.util.Log;

import com.harold.lastfmsearch.models.ApplicationData;
import com.harold.lastfmsearch.models.Constants;
import com.harold.lastfmsearch.models.ImageRequest;

import java.net.URL;
import java.util.LinkedList;

public class ImageManager {

    private static String TAG = ImageManager.class.getSimpleName().toString() + ".TAG";

    // Active Instance of this class
    private static ImageManager mInstance = null;

    // Bitmap memory cache
    private LruCache<String, Bitmap> mMemoryCache;

    // Total Available memory
    private final int m_iMaxMemory;

    // Max size of Bitmap cache
    private final int m_iCacheSize;

    //Image Request Queue
    private static LinkedList<ImageRequest> m_qImageRequests = new LinkedList<ImageRequest>();

    private ImageManager() {
        m_iMaxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
        m_iCacheSize = m_iMaxMemory / 4;

        mMemoryCache = new LruCache<String, Bitmap>(m_iCacheSize) {
            @Override
            protected int sizeOf(String key, Bitmap bitmap) {
                return bitmap.getByteCount() / 1024;
            }
        };
    }

    public static synchronized ImageManager getInstance() {
        if (mInstance == null)
            mInstance = new ImageManager();

        return mInstance;
    }

    /**
     * Load requested image from download or from cache
     * @param strImageID_URL Image URL
     * @return Bitmap to represent requested Image
     */
    public Bitmap loadImage(final String strImageID_URL) {
        final ImageRequest request = new ImageRequest();
        Bitmap bmpResult = null;
        boolean bAlredyRequested = false;

        // Check cache for image
        bmpResult = mMemoryCache.get(strImageID_URL);

        // if bitmap was not already loaded queue it up for download
        if (bmpResult == null) {
            // Check to see if image was already requested for download
            for (ImageRequest ir : m_qImageRequests) {
                if (ir.getImagePath().equals(strImageID_URL)) {
                    bAlredyRequested = true;
                    break;
                }
            }

            //
            if (!bAlredyRequested) {
                request.setImagePath(strImageID_URL);

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            URL url = new URL(strImageID_URL);
                            Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                            mMemoryCache.put(strImageID_URL, bmp);
                            m_qImageRequests.remove(request);
                            LocalBroadcastManager.getInstance(ApplicationData.getContext())
                                    .sendBroadcast(new Intent(Constants.BROADCAST_REFRESH_DATA));
                        }catch(Exception ex){
                            Log.d(TAG, ex.getMessage());
                        }
                    }
                }).start();

                m_qImageRequests.addLast(request);
            }
        }
        return bmpResult;
    }
}
