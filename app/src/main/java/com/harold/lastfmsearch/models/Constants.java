package com.harold.lastfmsearch.models;

/**
 * Created by frazierh on 6/30/15.
 */
public class Constants {
    //Broadcast id to refresh list data
    public static String BROADCAST_REFRESH_DATA = "broadcast.refresh.data";
}
