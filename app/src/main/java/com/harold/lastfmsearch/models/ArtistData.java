package com.harold.lastfmsearch.models;

/**
 * Created by frazierh on 6/30/15.
 */
public class ArtistData {
    // Artist Name
    private String mName = null;

    // URL of Artist Image
    private String mImageURL = null;

    // Unique Record id for artist
    private String mMBID = null;

    public ArtistData(){

    }

    /**
     * GEt Artist Name
     * @return Name of artist
     */
    public String getName() {
        return mName;
    }

    /**
     * Set Artist Name
     * @param name Name of Artist
     */
    public void setName(String name) {
        mName = name;
    }

    /**
     * Get artist image URL
     * @return URL of artist image
     */
    public String getImageURL() {
        return mImageURL;
    }

    /**
     * Set artist image URL
     * @param mImageLocation URL of artist image
     */
    public void setImageURL(String mImageLocation) {
        this.mImageURL = mImageLocation;
    }

    /**
     * Get unique record id
     * @return Last FM unique record id for artist
     */
    public String getMBID() {
        return mMBID;
    }

    /**
     * Set unique record id for artist
     * @param mMBID unique record id
     */
    public void setMBID(String mMBID) {
        this.mMBID = mMBID;
    }
}
