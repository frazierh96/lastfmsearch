package com.harold.lastfmsearch.models;

import android.app.Application;
import android.content.Context;

public class ApplicationData extends Application {

	// Application Context
	private static ApplicationData mInstance;

	public static Context getContext() {
		return mInstance;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		mInstance = this;
	}
}