package com.harold.lastfmsearch.models;

public class ImageRequest {
    //Requested image URL
    private String m_strImagePath;

	public ImageRequest() {
	}

    /***
     * Get image URL
     * @return Image URL
     */
    public String getImagePath() {
        return m_strImagePath;
    }

    /**
     * Set image URL
     * @param strImagePath Image URL
     */
    public void setImagePath(String strImagePath) {
        this.m_strImagePath = strImagePath;
    }
}