package com.harold.lastfmsearch.activity;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.SearchView;

import com.harold.lastfmsearch.R;
import com.harold.lastfmsearch.adapters.SearchResultsAdapter;


public class MainActivity extends Activity {
    // Adapter to show search results
    private SearchResultsAdapter mSearchResultsAdapter = null;

    // View for the the results list
    private RecyclerView rvSearchResults = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initializeGlobals();
        setupViews();
    }

    /**
     * All views are linked to method variables here
     */
    private void setupViews() {
        rvSearchResults = (RecyclerView)findViewById(R.id.activity_main_search_results_list);
        rvSearchResults.setLayoutManager(new LinearLayoutManager(this));
        rvSearchResults.setItemAnimator(new DefaultItemAnimator());
        rvSearchResults.setAdapter(mSearchResultsAdapter);
    }

    /**
     * All global variables are initialized here
     */
    private void initializeGlobals(){
        mSearchResultsAdapter = new SearchResultsAdapter();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        // We are using the actionbar search so we need to setup a listener for when a query
        // is submitted.
        SearchManager manager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView search = (SearchView) menu.findItem(R.id.search).getActionView();
        search.setSearchableInfo(manager.getSearchableInfo(getComponentName()));
        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                mSearchResultsAdapter.updateQueryString(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.search) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
