package com.harold.lastfmsearch.adapters;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.net.http.AndroidHttpClient;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.harold.lastfmsearch.R;
import com.harold.lastfmsearch.controlller.ImageManager;
import com.harold.lastfmsearch.models.ApplicationData;
import com.harold.lastfmsearch.models.ArtistData;
import com.harold.lastfmsearch.models.Constants;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;


public class SearchResultsAdapter extends RecyclerView.Adapter<SearchResultsAdapter.ViewHolder> {

    public static String TAG = SearchResultsAdapter.class.getSimpleName() + ".TAG";

    // Search Results Data container
    private ArrayList<ArtistData> aryArtistData;

    // Receiver to handle refreshing the list after a successful
    // query response from the server
    private BroadcastReceiver brRefreshData = null;

    //Total Avail records for this query
    private int iTotalResultSize = 0;

    //Current results are based this query
    private String mCurrentQuery = "";

    //Active HTTP Connection for query results
    private AndroidHttpClient httpClient = null;

    /***
     *
     */
    public SearchResultsAdapter() {
        initializeGlobals();
        setupReceivers();
    }

    private void setupReceivers() {
        brRefreshData = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                notifyDataSetChanged();
            }
        };
        LocalBroadcastManager.getInstance(ApplicationData.getContext()).registerReceiver
                (brRefreshData,
                        new IntentFilter(Constants.BROADCAST_REFRESH_DATA));
    }

    private void initializeGlobals() {
        aryArtistData = new ArrayList<>();
    }

    @Override
    public SearchResultsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;

        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.listitem_search_results, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(SearchResultsAdapter.ViewHolder holder, int position) {
        ArtistData data;
        URL url = null;
        Bitmap bmp = null;

        if (position >= aryArtistData.size()) {
            getNextSet();
            while (position >= aryArtistData.size()) {
                try {
                    Thread.sleep(2000);
                } catch (Exception ex) {

                }
            }
        }
        data = aryArtistData.get(position);

        holder.txtvName.setText(data.getName());
        holder.imgvImage.setImageBitmap(ImageManager.getInstance().loadImage(data.getImageURL()));
    }

    @Override
    public int getItemCount() {
        return iTotalResultSize;
    }

    /**
     * Update the query results of the adapter
     *
     * @param query New query to use to populate list
     */
    public void updateQueryString(String query) {
        final String strHTTPRequest = "http://ws.audioscrobbler.com/2.0/?method=artist" +
                ".search&artist=" + query + "&api_key=2b69d96818c66ff503872eff229537bd&format=json";

        //Clear current data and refresh list
        aryArtistData.clear();
        iTotalResultSize = 0;
        notifyDataSetChanged();

        mCurrentQuery = query;

        new Thread(new Runnable() {
            @Override
            public void run() {
                JSONObject response = restGet(strHTTPRequest);
                parseResults(response);
            }
        }).start();
    }

    /***
     * Request the next set of artist
     */
    private void getNextSet(){
        final String strHTTPRequest = "http://ws.audioscrobbler.com/2.0/?method=artist" +
                ".search&artist=" + mCurrentQuery +
                "&page=" + ((aryArtistData.size() / 30) + 1) +
                "&api_key=2b69d96818c66ff503872eff229537bd&format=json";

        new Thread(new Runnable() {
            @Override
            public void run() {
                JSONObject response = restGet(strHTTPRequest);
                parseResults(response);
            }
        }).start();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        protected ImageView imgvImage = null;
        protected TextView txtvName = null;

        public ViewHolder(View itemView) {
            super(itemView);

            txtvName = (TextView) itemView.findViewById(R.id.listitem_search_results_txtvName);
            imgvImage = (ImageView) itemView.findViewById(R.id.listitem_search_results_imgvImage);
        }
    }

    public JSONObject restGet(String url) {
        boolean bContinue = true;
        HttpResponse response = null;
        HttpGet get = null;
        JSONObject result = null;

        if (!url.startsWith("http")) {
            throw new RuntimeException("no http");
        }

        try {
            get = new HttpGet(url);
            get.addHeader("Accept", "text/xml");

            while (bContinue) {
                response = executeHTTPCommand(get);

                /*
                Check the response for success or failure.  If failure retry until success.
                 */
                if (response == null
                        || response.getStatusLine().getStatusCode() != 200) {
                    Thread.sleep(2000);
                } else if (response.getStatusLine().getStatusCode() == 200) {
                    bContinue = false;

                    BufferedReader reader = new BufferedReader(
                            new InputStreamReader(response.getEntity()
                                    .getContent(), "UTF-8"), 8);
                    StringBuilder sb = new StringBuilder();

                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    result = new JSONObject(sb.toString());
                }
            }
        } catch (Exception ex) {
            Log.d(TAG, ex.getMessage());
            result = null;
        }

        return result;
    }

    private HttpResponse executeHTTPCommand(HttpUriRequest request) {
        HttpResponse response = null;

        try {
            if (httpClient != null){
                httpClient.close();
                httpClient = null;
            }

            httpClient = AndroidHttpClient.newInstance("\"Mozilla/5.0 (Linux; U; Android 2" +
                    ".1; en-us; ADR6200 Build/ERD79) AppleWebKit/530.17 (KHTML, like Gecko) Version/ 4.0 Mobile Safari/530.17\"");
            response = httpClient.execute(request);
        } catch (Exception ex) {
            Toast.makeText(ApplicationData.getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
            response = null;
        }

        return response;
    }

    private void parseResults(JSONObject json) {
        JSONArray jaryArtistResults = null;
        JSONObject jsonObject = null;
        ArtistData artistData = null;
        int iOffset = 0;
        int iItemsPerPage = 0;
        boolean bAddingNewData = false;

        if (json == null) {
            return;
        }

        try {
            iTotalResultSize = json.getJSONObject("results").getInt("opensearch:totalResults");
            iOffset = json.getJSONObject("results").getInt("opensearch:startIndex");
            iItemsPerPage = json.getJSONObject("results").getInt("opensearch:itemsPerPage");

            if (iItemsPerPage == 1){
                iTotalResultSize = 1;
                jsonObject = json.getJSONObject("results").getJSONObject("artistmatches").
                        getJSONObject("artist");
                jaryArtistResults = new JSONArray();
                jaryArtistResults.put(jsonObject);
            }else {
                jaryArtistResults = json.getJSONObject("results").getJSONObject("artistmatches").
                        getJSONArray("artist");
            }

            if (iOffset == 0) {
                aryArtistData = new ArrayList<>();
            }

            if (iOffset >= aryArtistData.size()) {
                bAddingNewData = true;
            }

            for (int i = 0; i < jaryArtistResults.length(); i++) {
                artistData = new ArtistData();
                artistData.setName(jaryArtistResults.getJSONObject(i).getString("name"));
                artistData.setMBID(jaryArtistResults.getJSONObject(i).getString("mbid"));
                artistData.setImageURL(jaryArtistResults.getJSONObject(i).getJSONArray("image")
                        .getJSONObject(2).getString("#text"));

                if (bAddingNewData) {
                    aryArtistData.add(artistData);
                } else {
                    aryArtistData.set(i + iOffset, artistData);
                }
            }

            Intent intent = new Intent(Constants.BROADCAST_REFRESH_DATA);
            LocalBroadcastManager
                    .getInstance(ApplicationData.getContext())
                    .sendBroadcast(intent);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


}
